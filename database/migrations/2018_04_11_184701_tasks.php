<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('alias')->unique();
            $table->enum('status', ['up', 'down'])->default('up');
            $table->date('initialDate');
            $table->date('finalDate');
            $table->integer('spentTime');
            $table->integer('assigned_user')->unsigned();
            $table->integer('project_id')->unsigned();            
            $table->timestamps();

            $table->foreign('assigned_user')->references('id')->on('users');
            $table->foreign('project_id')->references('id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}

@extends('template.master')

@section('title', 'Lista de comentarios')

@section('content')

    <table class="table table-striped">
        <thead>
            <th>Titulo</th>
            <th>Comentario</th>
            <th>Etiquetas</th>    
            <th>Acciones</th>        
        </thead>    
        <tbody>
            @foreach($comments as $comment)
                <tr>
                    <td>{{$comment->title}}</td>
                    <td>{{$comment->comment}}</td>
                    <td>{{$comment->tags}}</td>                    
                    <td>
                        <button class="btn btn-danger">
                            <a href="{{route('comments.destroy', $comment->id)}}" onclick="confirm('¿Estas seguro que deseas eliminarlo?')">
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        </button>
                        <button class="btn btn-warning">
                            <a href="{{route('comments.edit', $comment->id)}}">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table> 

    {!! $comments->render() !!}

@endsection
@extends('template.master')

@section('title', 'Edición de comentario')

@section('content')
    <form class="form-horizontal" action="{{route('comments.update', $comment->id)}}" method="post">

        <div class="form-group">
            <div class="col-xs-12">
                <input type="hidden" name="_method" value="put" />
                <label>Título</label>                
                <input name="title" type="text" class="form-control" placeholder="Título" value="{{ $comment->title }}" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Comentario</label>
                <input name="comment" type="text" class="form-control" placeholder="comentario" value="{{ $comment->comment }}" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Etiquetas (separadas por comas)</label>
                <input name="tags" type="text" class="form-control" placeholder="Tags" value="{{ $comment->tags }}" required />
            </div>
        </div>       

        <div class="form-group">
            <div class="col-xs-12">
                <input class="btn btn-primary" type="submit" value="Enviar" />
            </div>
        </div>
        {{ csrf_field() }}
    </form>
@endsection
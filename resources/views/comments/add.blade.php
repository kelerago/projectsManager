@extends('template.master')

@section('title', 'Agregar comentario')

@section('content')
    <form class="form-horizontal" action="{{ route('comments.store') }}" method="post" required />
        <div class="form-group">
            <div class="col-xs-12">
                <label>Título</label>
                <input name="title" type="text" class="form-control" placeholder="Título" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Comentario</label>
                <input name="comment" type="text" class="form-control" placeholder="comentario" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Etiquetas (separadas por comas)</label>
                <input name="tags" type="text" class="form-control" placeholder="Tags" required />
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <label>Tarea</label>
                <select name="task_id" id="" class="form-control">
                    @foreach( $tasks as $task )
                        <option value="{{$task->id}}">{{$task->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-xs-12">
                <input name="user_id" type="hidden" value="1" class="form-control" placeholder="Fecha final">
            </div>
        </div>        

        <div class="form-group">
            <div class="col-xs-12">
                <input class="btn btn-primary" type="submit" value="Enviar">
            </div>
        </div>
        {{ csrf_field() }}
    </form>
@endsection
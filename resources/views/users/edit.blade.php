@extends('template.master')

@section('title', 'Edición de usuario')

@section('content')
    <form class="form-horizontal" action="{{route('users.update', $user->id)}}" method="post">
        <div class="form-group">
            <div class="col-xs-12">
                <input type="hidden" name="_method" value="put" />
                <label>Nombre</label>
                <input name="name" type="text" class="form-control" placeholder="Nombre" value="{{$user->name}}" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Correo Electrónico</label>
                <input name="email" type="email" class="form-control" placeholder="E-mail" value="{{$user->email}}" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Avatar</label>
                <input name="avatar" type="text" class="form-control" placeholder="avatar" value="{{$user->avatar}}" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Tipo</label>
                <select name="type" class="form-control" value="{{$user->type}}" required>
                    <option value="administrator">Administrador</option>
                    <option value="leader">líder</option>
                    <option value="registered">Registrado</option>
                </select>
            </div>
        </div>             

        <div class="form-group">
            <div class="col-xs-12">
                <input class="btn btn-primary" type="submit" value="Enviar" />
            </div>
        </div>
        {{ csrf_field() }}
    </form>
@endsection
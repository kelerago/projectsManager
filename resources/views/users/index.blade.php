@extends('template.master')

@section('title', 'Lista de Usuarios')

@section('content')

    <table class="table table-striped">
        <thead>
            <th>Nombre</th>
            <th>Correo Electrónico</th>
            <th>Avatar</th>
            <th>Tipo</th>            
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->avatar}}</td>
                    <td>{{$user->type}}</td>                    
                    <td>
                        <button class="btn btn-danger">
                            <a href="{{route('users.destroy', $user->id)}}" onclick="confirm('¿Estas seguro que deseas eliminarlo?')">
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        </button>
                        <button class="btn btn-warning">
                            <a href="{{route('users.edit', $user->id)}}">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table> 

    {!! $users->render() !!}

@endsection
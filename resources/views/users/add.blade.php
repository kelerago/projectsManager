@extends('template.master')

@section('title', 'Agregar usuario')

@section('content')
    <form class="form-horizontal" action="{{route('users.store')}}" method="post" required />
        <div class="form-group">
            <div class="col-xs-12">
                <label>Nombre</label>
                <input name="name" type="text" class="form-control" placeholder="Nombre" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Correo electrónico</label>
                <input name="email" type="email" class="form-control" placeholder="E-mail" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Contraseña</label>
                <input name="password" type="password" class="form-control" placeholder="Contraseña" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
            <label>Avatar</label>
                <input name="avatar" type="text" class="form-control" placeholder="Avatar" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Tipo</label>
                <select name="type" id="" class="form-control">
                    <option value="administrator">Administrador</option>
                    <option value="leader">Líder</option>
                    <option value="registered">Registrado</option>
                </select>
            </div>
        </div>       

        <div class="form-group">
            <div class="col-xs-12">
                <input class="btn btn-primary" type="submit" value="Enviar">
            </div>
        </div>
        {{ csrf_field() }}
    </form>
@endsection
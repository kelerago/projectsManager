<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title> @yield('title', 'Gestor de Proyectos')  </title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- Pickadate -->
    <link rel="stylesheet" href="{{asset('pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" href="{{asset('pickadate/lib/themes/default.date.css')}}">
    <script src="{{ asset('pickadate/lib/picker.js') }}"></script>
    <script src="{{ asset('pickadate/lib/picker.date.js') }}"></script>

    <!-- Fuente -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/scripts.js') }}"></script>
</head>
<body>

    <!-- Barra de navegación -->

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>    
                <a class="navbar-brand" href="/">
                    <img id="logo" class="img-responsive" src="https://useitweb.com/images/Home/logo.png" alt="">
                </a>        
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Usuarios <span class="caret"></span></a>
                        <ul class="dropdown-menu">                            
                            <li><a href="{{route('users.create')}}">Agregar</a></li>
                            <li><a href="{{route('users.index')}}">Ver</a></li>                        
                        </ul>
                    </li> -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                            Proyectos 
                            <img class="icons" src="{{ asset('img/edit-document.svg') }}" alt=""> 
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/admin/projects/create/">Agregar</a></li> 
                            <li><a href="/admin/projects/">Ver</a></li>                        
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                            Tareas 
                            <img class="icons" src="{{ asset('img/documents-symbol.svg') }}" alt="">
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('tasks.create') }}">Agregar</a></li>    
                            <li><a href="{{ route('tasks.index') }}">Ver</a></li>                                            
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                            Comentarios
                            <img src="{{ asset('img/opened-email-envelope.svg') }}" alt="" class="icons"> 
                            <span class="caret"></span>
                            </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('comments.create') }}">Agregar</a></li>
                            <li><a href="{{ route('comments.index') }}">Ver</a></li>                                                                 
                        </ul>
                    </li>                    
                </ul>          
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <!-- Fin barra de navegación -->

    <!--contenido -->

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> <h1>@yield('title')</h1> </div>
                    <div class="panel-body">
                        @include('flash::message')
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div> 
    
        

</body>
</html>
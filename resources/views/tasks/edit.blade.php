@extends('template.master')

@section('title', 'Edición de Tarea')

@section('content')
    <form class="form-horizontal" action="{{route('tasks.update', $task->id)}}" method="post">
        <div class="form-group">
            <div class="col-xs-12">                
                <input type="hidden" name="_method" value="put" />
                <labelNombre></label>
                <input name="name" type="text" class="form-control" placeholder="Nombre" value="{{$task->name}}" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Descripción</label>
                <input name="description" type="text" class="form-control" placeholder="descripción" value="{{$task->description}}" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Alias</label>
                <input name="alias" type="text" class="form-control" placeholder="Alias" value="{{$task->alias}}" required />
            </div>
        </div>        
        <div class="form-group">
            <div class="col-xs-12">
                <label>Fecha inicial</label>
                <input name="initialDate" type="text" class="date form-control" placeholder="Fecha Inicial" value="{{$task->initialDate}}" required />
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <label>Fecha final</label>
                <input name="finalDate" type="text" class="date form-control" placeholder="Fecha final" value="{{$task->finalDate}}" required />
            </div>
        </div> <div class="form-group">
            <div class="col-xs-12">
                <label>Tiempo Empleado</label>
                <input name="spentTime" type="text" class="form-control" placeholder="Tiempo empleado" value="{{$task->spentTime}}" required />
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <input class="btn btn-primary" type="submit" value="Enviar" />
            </div>
        </div>
        {{ csrf_field() }}
    </form>
@endsection
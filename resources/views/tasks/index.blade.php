@extends('template.master')

@section('title', 'Lista de Tareas')

@section('content')

    <table class="table table-striped">
        <thead>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Alias</th>
            <th>Estado</th>            
            <th>Fecha Inicial</th>
            <th>Fecha final</th>
            <th>Tiempo empleado</th>
            <th>Acciones</th>
        </thead>    
        <tbody>
            @foreach($tasks as $task)
                <tr>
                    <td>{{$task->name}}</td>
                    <td>{{$task->description}}</td>
                    <td>{{$task->alias}}</td>                    
                    <td>{{$task->status}}</td>
                    <td>{{$task->initialDate}}</td>
                    <td>{{$task->finalDate}}</td>
                    <td>{{$task->spentTime}}</td>
                    <td>
                        <button class="btn btn-danger">
                            <a href="{{route('tasks.destroy', $task->id)}}" onclick="confirm('¿Estas seguro que deseas eliminarlo?')">
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        </button>
                        <button class="btn btn-warning">
                            <a href="{{route('tasks.edit', $task->id)}}">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table> 

    {!! $tasks->render() !!}

@endsection
@extends('template.master')

@section('title', 'Gestor de Proyectos')

@section('content')

    <p id="welcomeText">
        Hola. Le damos la bienvenida a su gestor de proyectos básico. En el podra gestionarlos de forma facil.

        En la sección superior encontrara un menu que le permitira gestionar de forma básica algunos aspectos fundamentales de un proyecto.

        Que lo disfrutes.

        <center>
            <img src="{{ asset('img/settings-gears.svg') }}" alt="" class="icon-home">
        </center>
    </p>



@endsection
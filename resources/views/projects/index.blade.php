@extends('template.master')

@section('title', 'Lista de proyectos')

@section('content')

    <table class="table table-striped">
        <thead>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Avatar</th>
            <th>Alias</th>
            <th>Estado</th>
            <th>Fecha Inicial</th>
            <th>Fecha final</th>
            <th>Acciones</th>
        </thead>    
        <tbody>
            @foreach($projects as $project)
                <tr>
                    <td>{{$project->name}}</td>
                    <td>{{$project->description}}</td>
                    <td>{{$project->avatar}}</td>
                    <td>{{$project->alias}}</td>
                    <td>{{$project->status}}</td>
                    <td>{{$project->initialDate}}</td>
                    <td>{{$project->finalDate}}</td>
                    <td>
                        <button class="btn btn-danger">
                            <a href="{{route('projects.destroy', $project->id)}}" onclick="confirm('¿Estas seguro que deseas eliminarlo?')">
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        </button>
                        <button class="btn btn-warning">
                            <a href="{{route('projects.edit', $project->id)}}">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table> 

    {!! $projects->render() !!}

@endsection
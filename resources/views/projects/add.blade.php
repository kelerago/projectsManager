@extends('template.master')

@section('title', 'Agregar proyecto')

@section('content')
    <form class="form-horizontal" action="/admin/projects" method="post" required />
        <div class="form-group">
            <div class="col-xs-12">
                <label>Nombre</label>
                <input name="name" type="text" class="form-control" placeholder="Nombre" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Descripción</label>
                <input name="description" type="text" class="form-control" placeholder="descripción" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Avatar</label>
                <input name="avatar" type="text" class="form-control" placeholder="avatar" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Alias</label>
                <input name="alias" type="text" class="form-control" placeholder="Alias" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Fecha inicial</label>
                <input name="initialDate" type="text" class="date form-control" placeholder="Fecha Inicial" required />
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <label>Fecha final</label>
                <input name="finalDate" type="text" class="date form-control" placeholder="Fecha final" required />
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <input name="user_id" type="hidden" value="1" class="form-control" placeholder="Fecha final">
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <input class="btn btn-primary" type="submit" value="Enviar">
            </div>
        </div>
        {{ csrf_field() }}
    </form>
@endsection
@extends('template.master')

@section('title', 'Edición de proyecto ' . $project->name)

@section('content')
    <form class="form-horizontal" action="{{route('projects.update', $project->id)}}" method="post">
        <div class="form-group">
            <div class="col-xs-12">                
                <input type="hidden" name="_method" value="put" />
                <labelNombre></label>
                <input name="name" type="text" class="form-control" placeholder="Nombre" value="{{$project->name}}" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Descripción</label>
                <input name="description" type="text" class="form-control" placeholder="descripción" value="{{$project->description}}" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Avatar</label>
                <input name="avatar" type="text" class="form-control" placeholder="avatar" value="avatar" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Alias</label>
                <input name="alias" type="text" class="form-control" placeholder="Alias" value="{{$project->alias}}" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label>Fecha inicial</label>
                <input name="initialDate" type="date" class="date form-control" placeholder="Fecha Inicial" value="{{$project->initialDate}}" required />
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <label>Fecha final</label>
                <input name="finalDate" type="date" class="date form-control" placeholder="Fecha final" value="{{$project->finalDate}}" required />
            </div>
        </div>       

        <div class="form-group">
            <div class="col-xs-12">
                <input class="btn btn-primary" type="submit" value="Enviar" />
            </div>
        </div>
        {{ csrf_field() }}
    </form>
@endsection
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $table = "comments";

    protected $fillable = [
        'title', 'comment', 'tags', 'task_id', 'user_id'
    ];

    public function task(){

        return $this->belongsTo('App\Task');
    }
}

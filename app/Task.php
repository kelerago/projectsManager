<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = "tasks";

    protected $fillable = [
        'name', 'description', 'alias', 'status', 'initialDate', 'finalDate', 'spentTime', 'assigned_user', 'project_id'
    ];

    public function project(){

        return $this->belongsTo('App\Project');
    }

    public function comments(){

        return $this->hasMany('App\Comment');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = "projects";

    protected $fillable = [
        'name', 'description', 'avatar', 'alias', 'status', 'initialDate', 'finalDate', 'user_id'
    ];

    public function user(){

        return $this->belongsTo('App\User');
    }

    public function tasks(){

        return $this->hasMany('App\Task');
    }
}

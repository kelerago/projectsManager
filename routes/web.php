<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin'], function(){

    // Projects

    Route::resource('projects', 'ProjectsController');

    Route::get('projects/destroy/{id}', [

        'uses'  => "ProjectsController@destroy",
        'as' => "projects.destroy"
    ]);

    // Users

    Route::resource('users', 'UsersController');

    Route::get('users/destroy/{user}', [

        "uses" => "UsersController@destroy",
        "as" => "users.destroy"
    ]);

    // Tareas

    Route::resource('tasks', 'TasksController');

    Route::get('tasks/destroy/{task}', [

        "uses" => "TasksController@destroy",
        "as" => "tasks.destroy"
    ]);

    // Comentarios

    Route::resource('comments', 'CommentsController');

    Route::get('comments/destroy/{comment}', [

        "uses" => "CommentsController@destroy",
        "as" => "comments.destroy"
    ]);

});
